#!/usr/bin/env python3
# ##############################################################################
# cgtIVTHelper.py - Simple extraction and modification of binary IVT/DCD data
# Copyright (C) 2016 Alexander Pockes <alexander.pockes@congatec.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ##############################################################################


# -->
# imports
# --->

import os
import sys
from optparse import OptionParser


# -->
# constants
# --->

# version information
VERSION_MAJ = 0
VERSION_MIN = 3

# locating/identifying DCD/IVT header
HEADER_TAG_IVT = 0xD1
HEADER_TAG_DCD = 0xD2

# offset of HEADER_TAG_DCD + 1 = fileoffset of dcd length field (dcd header)
DCD_HEADER_LENGTH_OFFSET = 1
DCD_HEADER_DCDLENGTH_SIZE = 2

# offset between ivt header and dcdptr
DCD_PTR_OFFSET = 3 * 4
# DCD pointer size is 4 byte
DCD_PTR_SIZE = 4


# -->
# function definitions
# --->

def find_header_offset(file_obj, header_tag):
    """Find the offset of the IVT or DCD header of a given u-boot binary image file

    Keyword arguments:
    file_obj   -- file object returned by an open() call
    header_tag -- specification of the offset type to find;
                  IVT header -> HEADER_TAG_IVT = 0xD1
                  DCD header -> HEADER_TAG_DCD = 0xD2

    Return value:
    int : offset to specified header
    """
    HEADER_HAB_VERSION0 = 0x40
    HEADER_HAB_VERSION1 = 0x41

    chunk_size = 4
    offset = 0

    if file is None:
        return None

    try:
        file_obj.seek(0, 0)

        while True:
            br = file_obj.read(chunk_size)
            if (br[0] == header_tag and
                (br[3] == HEADER_HAB_VERSION0 or
                 br[3] == HEADER_HAB_VERSION1)):
                break
            offset += chunk_size

    except:
        print("ERR: reading input file failed")
        return None

    return offset


def get_bin_hex_data(file_obj, file_offset, byte_count):
    """binary read <byte_count> amount of bytes from file <file_obj>
    starting at offset <file_offset>

    Keyword arguments:
    file_obj    -- file object returned by an open() call
    file_offset -- specification of the offset to start reading;
    byte_count  -- number of bytes to read

    Return value:
    None  : on fail
    bytes : read binary data
    """
    if file_obj is None:
        return None

    try:
        file_obj.seek(file_offset, 0)
        br = file_obj.read(byte_count)
    except:
        return None

    return br


def get_dcdlength(file_obj):
    """extracting/returning the length of the dcdarea

    Keyword arguments:
    file_obj    -- file object returned by an open() call

    Return value:
    bytes : length of the dcd area (extracted from the dcd header)
    """
    dcdheader_offset = find_header_offset(file_obj, HEADER_TAG_DCD)
    dcdlength_addr = dcdheader_offset + DCD_HEADER_LENGTH_OFFSET
    dcdlength = get_bin_hex_data(file_obj, dcdlength_addr,
                                 DCD_HEADER_DCDLENGTH_SIZE)
    return dcdlength


def get_dcd_habblocks(file_obj):
    """extracting/returning the dcd-habblocks required in order to sign
    mfg u-boot images

    Keyword arguments:
    file_obj    -- file object returned by an open() call

    Return value:
    (int, bytes) : (offset to dcd header, length of dcd area)
    """
    dcdheader_offset = find_header_offset(file_obj, HEADER_TAG_DCD)
    dcdlength_addr = dcdheader_offset + DCD_HEADER_LENGTH_OFFSET
    dcdlength = get_bin_hex_data(file_obj, dcdlength_addr,
                                 DCD_HEADER_DCDLENGTH_SIZE)
    return (dcdheader_offset, dcdlength)


def get_dcdptr(file_obj, dcdptr_offset):
    """extracing/returning the pointer to the dcd area

    Keyword arguments:
    file_obj      -- file object returned by an open() call
    dcdptr_offset -- offset to the dcdptr, returned by find_header_offset

    Return value:
    bytes : ptr to/addr of dcd area
    """
    return get_bin_hex_data(file_obj, dcdptr_offset, DCD_PTR_SIZE)


def set_bin_hex_data(file_obj, file_offset, ba_hex_data):
    """binary writing data <ba_hex_data> (bytearray) to file <file_obj>
    starting at offset <offset>

    Keyword arguments:
    file_obj    -- file object returned by an open() call
    file_offset -- fileoffset to start writing
    ba_hex_data -- data (bytearray) to write to file <file_obj> at
                   offset <file_offset>
    """
    if file_obj is None:
        return None

    if not isinstance(ba_hex_data, bytearray):
        raise TypeError

    file_obj.seek(file_offset, 0)
    file.write(ba_hex_data)
    file.flush()


def print_bin_hex_data(bin_hex_data):
    """prints a bytes object hexadecimal to stdout.
    printing byte per byte without prefix (0x) or seperators (spaces)

    Keyword arguments:
    bin_hex_data -- binary data (bytes) to print to stdout
    """
    # print("bin_hex_data: ", end="")
    for i in bin_hex_data:
        print("{:02x}".format(i), end="")
    print("")


def clear_dcdptr(file_obj, ivtheader_offset):
    """clearing dcd ptr of binary image file <file_obj>

    Keyword arguments:
    file_obj    -- file object returned by an open() call
    """
    # list of \0 bytes in order to clear/set the dcd ptr to 0x00 0x00 0x00 0x00
    EMPTY_DCD_PTR_VAL_LIST = [0x0, 0x0, 0x0, 0x0]

    if file_obj is None:
        return None

    set_bin_hex_data(file_obj, ivtheader_offset + DCD_PTR_OFFSET,
                     bytearray(EMPTY_DCD_PTR_VAL_LIST))


def slice_str_every_n_chars(str, n):
    """slicing string <str> at every <n> chars

    Keyword arguments:
    str -- string to slice
    n   -- slice length

    Return Value:
    list : list of <n>-chars
    """
    return [str[i:i+n] for i in range(0, len(str), n)]


def conv_hexstr_to_bytearray(str):
    """convertig a hexadecimal string (e.g. 0x1234) to a bytearray
    of single bytes (\b'12', \b'34')

    Keyword arguments:
    str -- hexadecimal string presentation to convert

    Return Value:
    bytearray : single bytes...
    """
    return bytearray([int(i, 16) for i in slice_str_every_n_chars(str, 2)])


# -->
# class definitions
# --->

class IVTstruct():
    """ representing an image's binary IVT structure """
    # size of ivt area in bytes; 8 x 4 byte registers
    __IVT_STRUCT_LEN = 8 * 4
    # register size in bytes
    __REG_SIZE_BYTES = 4

    def __init__(self, file_obj, ivt_offset):
        self.__file_obj = file_obj
        self.__ivt_offset = ivt_offset

        self.__bytes_data = None
        self.__dict_data = {}
        self.__dict_rdata = {}
        self.DICT_KEYS = ["header",
                          "entry",
                          "reserved1",
                          "dcd",
                          "boot_data",
                          "self",
                          "csf",
                          "reserved2"]

    def __conv_rawbytes_to_fdict(self):
        CHUNK_SIZE = 4

        if self.__bytes_data is None:
            return False

        counter = 0
        for key in self.DICT_KEYS:
            self.__dict_data[key] = \
                self.__bytes_data[counter:counter+(CHUNK_SIZE)]
            counter += CHUNK_SIZE

        return True

    def __reverse_dict_data(self):
        if len(self.__dict_data) == 0:
            return False

        for key in self.__dict_data.keys():
            self.__dict_rdata[key] = self.__dict_data[key][::-1]

        return True

    def read(self):
        ret_val = False
        try:
            self.__file_obj.seek(self.__ivt_offset, 0)
            self.__bytes_data = self.__file_obj.read(IVTstruct.__IVT_STRUCT_LEN)
        except:
            return ret_val

        ret_val = self.__conv_rawbytes_to_fdict()

        if ret_val:
            ret_val = self.__reverse_dict_data()

        return ret_val

    def get_bytes(self):
        if self.__bytes_data is None:
            return None
        return self.__bytes_data

    def get_rdata_self(self):
        try:
            return self.__dict_rdata["self"]
        except:
            return None

    def print_bytedump(self):
        if self.__bytes_data is None:
            return False

        counter = 0
        for i in self.__bytes_data:
            print("{:02x}".format(int(i)), end="")
            counter += 1
            if (counter % 4) == 0:
                print("")

        return True

    def print_formatted_bytedump(self, dict_data):
        if(self.__bytes_data is None or
           len(dict_data) == 0):
            return False

        for key in self.DICT_KEYS:
            print("{:10} : ".format(key), end="")
            for byte in dict_data[key]:
                print("{:02x}".format(byte), end="")
            print("")

        return True

    def print_revdesc_bytedump(self):
        return self.print_formatted_bytedump(self.__dict_rdata)

    def print_desc_bytedump(self):
        return self.print_formatted_bytedump(self.__dict_data)


# -->
# main
# --->

if __name__ == "__main__":
    # variable definitions
    ivtheader_offset = None

    # creating cmdl arguments parser object
    cmd_arg_parser = OptionParser("{} [OPTIONS]".format(sys.argv[0]))
    # adding cmdl options
    cmd_arg_parser.add_option("-f",
                              "--filename",
                              dest="arg_filename",
                              type="string",
                              action="store",
                              help="specify u-boot binary filename")
    cmd_arg_parser.add_option("--get-dcdptr",
                              dest="action_print_dcdptr",
                              action="store_true",
                              help="print DCD pointer to stdout")
    cmd_arg_parser.add_option("--set-dcdptr",
                              dest="action_set_dcdptr",
                              action="store_true",
                              help="set DCD pointer to the value specified \
by --value")
    cmd_arg_parser.add_option("--value",
                              dest="arg_value",
                              action="store",
                              type="string",
                              help="specify --set-dcdptr input value")
    cmd_arg_parser.add_option("--clear-dcdptr",
                              dest="action_clear_dcdptr",
                              action="store_true",
                              help="clear/set the DCD pointer to 00000000")
    cmd_arg_parser.add_option("--get-ivtheader-offset",
                              dest="action_print_ivtheader_offset",
                              action="store_true",
                              help="print IVT header offset to stdout")
    cmd_arg_parser.add_option("--get-dcdlength",
                              dest="action_print_dcdlength",
                              action="store_true",
                              help="print length of DCD area to stdout")
    cmd_arg_parser.add_option("--get-ivtstruct",
                              dest="action_print_ivtstruct",
                              action="store_true",
                              help="print IVT structure to stdout")
    cmd_arg_parser.add_option("--format",
                              dest="arg_format",
                              action="store",
                              type="string",
                              help="specify output format of --get-ivtstruct;\
                              output formats: <plain>, <desc>, <revdesc>")
    cmd_arg_parser.add_option("--get-habblocks",
                              dest="action_print_habblocks",
                              action="store_true",
                              help="print HAB blocks to stdout")
    cmd_arg_parser.add_option("--get-dcd-habblocks",
                              dest="action_print_dcd_habblocks",
                              action="store_true",
                              help="print DCD HAB blocks to stdout;\
                              required in order to sign a mfg u-boot image")
    cmd_arg_parser.add_option("--version",
                              dest="action_print_version_info",
                              action="store_true",
                              help="print version information to stdout")

    # trigger parsing of cmdl options/arguments
    (cmd_options, cmd_args) = cmd_arg_parser.parse_args()

    # check if there is a filename given via -f
    if cmd_options.arg_filename:
        filename = cmd_options.arg_filename
        try:
            file = open(filename, "r+b")
        except:
            print("ERR: could not open file {}, exiting...".format(filename))
            exit(1)
    else:
        # action: action_print_version_info
        if cmd_options.action_print_version_info:
            print("{} v{}.{}".format(sys.argv[0], VERSION_MAJ, VERSION_MIN))
            exit(0)

        cmd_arg_parser.print_help()
        exit(1)

    # (1) finding ivtheader / ivtheader_offset
    if(cmd_options.action_print_ivtheader_offset or
       cmd_options.action_print_dcdptr or
       cmd_options.action_clear_dcdptr or
       cmd_options.action_print_ivtstruct or
       cmd_options.action_set_dcdptr or
       cmd_options.action_print_habblocks):
        # getting offset
        ivtheader_offset = find_header_offset(file, HEADER_TAG_IVT)

    # (2) perform action specified by commandline

    # action: action_print-ivtheader_offset
    if cmd_options.action_print_ivtheader_offset:
        if ivtheader_offset is not None:
            # print("IVTHeaderOffset: 0x{:X}".format(ivtheader_offset))
            print("0x{:X}".format(ivtheader_offset))
        else:
            print("FAIL: could not find IVT Header offset, exiting...")
            exit(2)

    # action: action_print_dcdptr
    if cmd_options.action_print_dcdptr:
        dcdptr = get_dcdptr(file, ivtheader_offset + DCD_PTR_OFFSET)
        print_bin_hex_data(dcdptr)

    # action: action_print_dcdlength
    if cmd_options.action_print_dcdlength:
        dcdlength = get_dcdlength(file)
        print_bin_hex_data(dcdlength)

    # action: action_print_dcd_habblocks
    if cmd_options.action_print_dcd_habblocks:
        dcdlength_offset, dcdlength = get_dcd_habblocks(file)
        print("DCD offset : 0x{:08x}{}".format(dcdlength_offset, 5*" "))
        print("DCD length : 0x", end="")
        print_bin_hex_data(dcdlength)

    # action: action_clear_dcdptr
    if cmd_options.action_clear_dcdptr:
        clear_dcdptr(file, ivtheader_offset)

    # action: set_dcdptr
    if cmd_options.action_set_dcdptr:
        if (not cmd_options.arg_value or
                len(cmd_options.arg_value) != 8):
            print("ERR: please specify dcdptr \
value to set via --value XXXXXXXX, exiting...")
            exit(1)

        ba_dcdptr = conv_hexstr_to_bytearray(cmd_options.arg_value)
        print("INFO: setting dcdptr to: ", end="")
        for i in ba_dcdptr:
            print("0x{:02x} ".format(i), end="")
        print("")

        set_bin_hex_data(file, ivtheader_offset + DCD_PTR_OFFSET, ba_dcdptr)

    # action: action_print_ivtstruct
    if cmd_options.action_print_ivtstruct:
        ivtstruct_obj = IVTstruct(file, ivtheader_offset)
        if ivtstruct_obj.read():
            if cmd_options.arg_format == "desc":
                ivtstruct_obj.print_desc_bytedump()
            elif cmd_options.arg_format == "revdesc":
                ivtstruct_obj.print_revdesc_bytedump()
            else:
                ivtstruct_obj.print_bytedump()
        else:
            print("ERR: reading ivtstruct failed, exiting...")
            exit(3)

    # action: action_print_habblocks
    if cmd_options.action_print_habblocks:
        habblocks = {}
        # (1) obtaining "self"
        ivtstruct_obj = IVTstruct(file, ivtheader_offset)
        if ivtstruct_obj.read():
            habblocks["start-signing-addr"] = ivtstruct_obj.get_rdata_self()
        else:
            print("ERR: reading ivtstruct failed, exiting...")
            exit(3)

        # (2) obtaining ivtheader offset
        habblocks["start-signing-offset"] = ivtheader_offset

        # (3) obtaining length of data to sign
        file_stat = os.stat(filename)
        file_length = file_stat.st_size
        habblocks["signed-data-length"] = file_length - ivtheader_offset

        # (4) printing habblocks to stdout
        print("start-signing address : 0x", end="")
        print_bin_hex_data(habblocks["start-signing-addr"])
        print("start-signing offset  : 0x{:x}".format(\
            habblocks["start-signing-offset"]))
        print("signed-data length    : 0x{:x}".format(\
            habblocks["signed-data-length"]))

    # (3) cleaning up...
    if file:
        file.close()

    exit(0)
